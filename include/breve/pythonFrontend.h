
#include "config.h"
#ifdef HAVE_LIBPYTHON

#include <signal.h>

#include "kernel.h"

#include <Python.h>

#define PYTHON_TYPE_SIGNATURE	0x7079746f

void brPythonInit( brEngine *breveEngine );

#endif
